﻿#include <iostream>
#include <string>
using namespace std;


class PlayerData
{
private:

	string Name;
	int Score;

public:

	PlayerData() : Name("Un"), Score(0)
	{}
	
	PlayerData(string _Name, int _Score) : Name(_Name), Score(_Score)
	{}

	void Show()
	{
		cout << '\n' << Name << " : " << Score << endl;
	}

	int GetScore()
	{
		return Score;
	}

	void SetScore(int newScore)
	{
		Score = newScore;
	}

	string GetName()
	{
		return Name;
	}

	void SetName(string newName)
	{
		Name = newName;
	}

	void SetPlayer(string newName, int newScore)
	{
		Name = newName;
		Score = newScore;
	}
};

void SelectionSort(PlayerData* base, int size)
{
	int j;
	PlayerData tmp;
	for (int i = 0; i < size; i++)
	{
		j = i;
		for (int k = i; k < size; k++)
		{
			if (base[j].GetScore() > base[k].GetScore())
			{
				j = k;
			}
		}
		tmp = base[i];
		base[i] = base[j];
		base[j] = tmp;
	}
}

int main()
{
	cout << "How many players do we have?" << endl;

	int HowManyPlayers(0);
	cin >> HowManyPlayers;

	string arrName;
	int arrScore;

	PlayerData* base = new PlayerData[HowManyPlayers];

	for (int i = 0, j = 0; i < HowManyPlayers; ++i, j++)
	{
		string y;
		int x;

		cout << "Enter " << i + 1 << " name:";
		cin >> y;
		arrName = y;

		cout << "Enter " << y << " score:";
		cin >> x;
		arrScore = x;

		base[i].SetPlayer(arrName, arrScore);
	}

	cout << "\nYou enter: \n";
	for (int i = 0; i < HowManyPlayers; i++)
	{
		base[i].Show();
	}

	SelectionSort(base, HowManyPlayers);
	cout << endl;

	for (int i = 0; i < HowManyPlayers; i++)
	{
		base[i].Show();
	}

	return 0;
}